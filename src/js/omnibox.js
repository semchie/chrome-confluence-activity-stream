var currentRequest = null;
var dump;

chrome.omnibox.onInputChanged.addListener(function(text, suggest) {
//  if (currentRequest != null) {
//    currentRequest.onreadystatechange = null;
//    currentRequest.abort();
//    currentRequest = null;
//  }

//	updateDefaultSuggestion(text);

	currentRequest = search(text, function(json) {
		var results = [];
    var data = JSON.parse(json);


    var types = data.contentNameMatches;
    for (var i = 0, type; i < types.length && (type = types[i]); i++) {
      var entries = type;
      for (var j = 0, entry; j < entries.length && (entry = entries[j]); j++) {
        if (entry.className === 'search-for') continue;
        var className = entry.className.split('-').splice(-1);
        var description = '<dim>[' + className + ']</dim> <match>' + entry.name + '</match> <url>https://extranet.atlassian.com' + entry.href + '</url>';
        var content = "https://extranet.atlassian.com" + entry.href;

        var result = {
          content: content.replace(/\&/g,"&amp;"),
          description: description.replace(/\&/g,"&amp;")
        };
        results.push(result);
      }
    }
    suggest(results);
	});
});

function resetDefaultSuggestion() {
  chrome.omnibox.setDefaultSuggestion({
    description: '<dim>Search EAC with [query]</dim>'
  });
}

resetDefaultSuggestion();

function updateDefaultSuggestion(text) {
  chrome.omnibox.setDefaultSuggestion({
    description: '<dim>Search EAC with:</dim><match>%s</match>'
  });
}

chrome.omnibox.onInputStarted.addListener(function() {
  updateDefaultSuggestion('');
});

chrome.omnibox.onInputCancelled.addListener(function() {
  resetDefaultSuggestion();
});

function search(query, callback) {
  if (query.length < 3) return;
	var url = "https://extranet.atlassian.com/json/contentnamesearch.action?query=" + query;
  url = encodeURI(url);


	var req = new XMLHttpRequest();
	req.open("GET", url, true);
	req.setRequestHeader("Accept", "application/json");
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			callback(req.responseText);
		}
	}
	req.send(null);
	return req;
}

function navigate(url) {
  if (!/^http/.test(url)) {
    url = encodeURI("https://extranet.atlassian.com/dosearchsite.action?queryString=" + url);
  }

	chrome.tabs.getSelected(null, function(tab) {
		chrome.tabs.update(tab.id, {
			url: url
		});
	});
}

chrome.omnibox.onInputEntered.addListener(navigate);

